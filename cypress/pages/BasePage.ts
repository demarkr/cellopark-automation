export class BasePage {
	private searchBarField = '.searchBar';
	private searchBtn = '.searchButton';
	private autoCompleteText = '.autoComplete';
	private searchErrorMessage = '.errorMessage';

	constructor() {}

	public get SearchBar(){
		return cy.get(this.searchBarField);
	}

	public get SearchButton(){
		return cy.get(this.searchBtn);
	}

	public get AutoCompleteText(){
		return cy.get(this.autoCompleteText);
	}

	public get SearchErrorMessage(){
		return cy.get(this.searchErrorMessage);
	}

	public Search(searchTerm : string){
		this.SearchBar.type(searchTerm);
		this.SearchButton.click();
	}
}