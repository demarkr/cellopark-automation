import { BasePage } from "./BasePage";

export class HomePage extends BasePage {
	homePageBanner = '.logo';

	constructor() {
		super();

		// Make sure we are on the home page.
		this.HomePageBanner.should('be.visible');
	}

	public get HomePageBanner(){
		return cy.get(this.homePageBanner);
	}

	// Insert further functionality that exists in the Home Page.
}