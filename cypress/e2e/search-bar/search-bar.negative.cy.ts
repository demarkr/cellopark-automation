import { HomePage } from "../../pages/HomePage";
import 'cypress-each';

describe('Search bar negative tests', ()=>{
	var homePage : HomePage;

	before(()=>{
		cy.visit('/'); // Should visit baseUrl defined in the cypress.config.js file
		homePage = new HomePage();
	});

	// Data-driven testing, %s in the title is replaced by the value being tested.
	it.each(['123','!@#$%^','  '])('Search invalid characters: %s', (value)=>{
		homePage.SearchBar.type(value);
		homePage.AutoCompleteText.should('have.value', '');
		homePage.SearchButton.click();

		// Assertions
		homePage.SearchErrorMessage.should('be.visible');
		homePage.SearchErrorMessage.should('contain.text', 'Only A-Z letters are allowed!');
		homePage.HomePageBanner.should('be.visible'); // Make sure we are still on the home page and the search didn't happen.
	})
});